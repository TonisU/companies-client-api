//suhtlus serveriga. Tühi praegu.

async function fetchCompanies() {
    try {
        let response = await fetch(API_URL + "/companies"); // võib ka aadressi API_URL otse muuta config.js lehel
        let companies = await response.json();
        return companies;
    } catch (e) {
        console.log("Kuskil on jama!", e);
    }
}

async function fetchOneCompany(id) {
    try {
        let response = await fetch(API_URL + "/companies/" + id);
        let company = await response.json();
        return company;
    } catch (e) {
        console.log("Ettevõtte laadimine ebaõnnestus!", e);
    }
}

async function deleteCompany(id) {
    let result = await fetch(API_URL + "/companies/" + id, {
        method: 'DELETE',
    });
}

async function saveCompany (company) {
    let requestURL = API_URL + "/companies";
    let conf = {
        method: 'Post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(company)
    };
    let result = await fetch(requestURL, conf);
}